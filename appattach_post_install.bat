@ECHO OFF

echo Beginning script on %date% at %time%

cd system

cd etc
echo .
echo Step 1. Push all files in the etc folder to /data/local/tmp/
for /R %%s in (*) do (
echo Pushing %%s
adb push "%%s" /data/local/tmp/
)

set devicemanager_cfg_found=0
echo .
echo push completed. Check for /data/local/tmp/devicemanager.cfg...
for /f "tokens=1" %%i in ('adb shell ls /data/local/tmp/ ^| findstr "devicemanager.cfg"') do (
	set devicemanager_cfg_found=1
	echo SUCCESS: the Device Manager config file was placed correctly.
)
if %devicemanager_cfg_found% == 0 (
echo -- ERROR:
echo -- the Device Manager config file was not found in the /data/local/tmp/ directory. 
echo -- This probably means the file didn't get pushed correctly.
)


pause

cd ..
cd aapreinstall
echo .
echo Step 2. Install all APKs in the app folder
for /R %%s in (*.apk) do (
echo Installing %%s
adb install -r "%%s"
)

cd ..
cd app
echo .
echo Step 3. Install all APKs in the apps folder
for /R %%s in (*.apk) do (
echo Installing %%s
adb install -r "%%s"
)

REM echo Turning on debugging.
REM adb shell "am broadcast -a DEBUG_ON -n com.devicemanager/com.devicemanager.Receiver"

echo .
echo Sending ALARM broadcast to DeviceManager as a wakeup.
adb shell "am broadcast -a ALARM -n com.devicemanager/.Receiver"

REM ECHO installation complete press any key to power off the device.

pause
REM adb shell "reboot -p"