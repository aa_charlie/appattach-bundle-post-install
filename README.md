# appAttach Post-Manufacture Apps Installation Script #

This script is intended to show the process for loading an appAttach apps bundle on a device that has already been manufactured. If possible, it is far better to load the app bundle assets to the /system folder of the production image ROM. This script and method should only be used where root access and ability to modify the ROM is not easily obtained. It is common to use this method for pre-production testing to prove concept and explore user experience where root access is not available.

### Process ###

* The intended process is as follows:
    1. Boot device.
    2. Run Script.
    3. Power device off.
    4. Re-package device.
    5. Device activation will be sent on the next boot.
 

### What are the drawbacks of using this method compared to the recommended method? ###

* This method is not officially supported, (although we will do our best to help however we can) development efforts will be focused on ROM-inclusion.
* All files not in /system folder are removed during a factory reset which may decrease your revenue and may result in an inconsistent user experience if the apps that were there when they unboxed the device are gone after a factory reset.
* Modifying a ROM and flashing across many devices is generally less effort per device than running this script on each device.


### Before you start you will need ###

* A test Android device.
* A Windows PC with:
    * [ADB](http://developer.android.com/tools/help/adb.html)
    * ADB set in the Windows PATH so it can be called from any folder.
    * The correct ADB drivers for your device.
* Your appAttach apps bundle (If you don't have one, please contact your appAttach representative).
* The appattach_post_install.bat from this repository. [Download repository](https://bitbucket.org/aa_charlie/appattach-bundle-post-install/downloads)


### Usage ###

1. Extract the appAttach apps bundle.
2. Place appattach_post_install.bat in the same folder as the "system" folder from your appAttach bundle.
3. Run appattach_post_install.bat
4. After script completes, power the device off.


### Verification ###

In normal production, we would stop at the step 4 above and box the device up for shipment but if this is a test, we need to simulate end user behavior and make sure tracking works properly.

1. Power the device on.
2. Wait for the device to boot.
3. connect to the internet.
4. Wait about 10 minutes. (Make sure the device's internet connection does not drop because of sleeping)
5. Check the test tracker for events:
    * Test tracker: [http://devmgr.net/testing](http://devmgr.net/testing)
    * Username: testing
    * Password: dmTesting
    * Enter the MAC address of your device to show the events.